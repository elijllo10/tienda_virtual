$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 4000
    });

    $('#contactenos').on('show.bs.modal', function (e) {
    console.log('el modal se está mostrando');
        $('#contactoBtn').removeClass('btn-outline-dark');
        $('#contactoBtn').addClass('btn-secondary');
        $('#contactoBtn').prop('disabled',true);
    });
    
    $('#contactenos').on('shown.bs.modal', function (e) {
    console.log('el modal se mostró');
    });
    
    $('#contactenos').on('hide.bs.modal', function (e) {
    console.log('el modal se está ocultando');
    });

    $('#contactenos').on('hidden.bs.modal', function (e) {
    console.log('el modal se está ocultó');
    $('#contactoBtn').prop('disabled',false);
    $('#contactoBtn').removeClass('btn-secondary');
    $('#contactoBtn').addClass('btn-outline-dark');
    });
});